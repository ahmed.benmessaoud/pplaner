# -*- coding: utf-8 -*-
{
    'name': "Planification",
    'summary': "Odoo can plan for you",
    'description': """
    this module permit you to prepare your planification easily
    """,
    'author': "Ahmed B.Messaoud & Med Ali Kefi",
    'category': "Management",
    'application': "true",
    'version': "0.1",
    'licence': "GPL-3",
    'application' : "true",
    'depends': ['base','project'],
    'data': [
            'views/planification_data.xml',
            'views/tache.xml',
            'report/plan_report.xml',
            'report/task_report.xml'
             ],
    'demo':[],
    }