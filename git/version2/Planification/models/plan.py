# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import date
class Plan(models.Model):
    _name = 'planification.plan'
    _description = 'Plan de Charge'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _defaults = { 'ref': lambda self,cr,uid,context: self.pool.get('ir.sequence').get(cr, uid, 'planification.plan') }
    
    nom=fields.Char(required=True)
    client=fields.Many2one(comodel_name='res.partner',required=True)
    date_start = fields.Date(default=fields.Date.today(),index=True, copy=False)
    version = fields.Selection([('a','1.0'),('b','2.0'),('c','3.0'),('d','4.0'),('e','5')])
    ref=fields.Char('Ref',readonly=True)
    hjs=fields.Integer(required=True)
    totalot=fields.Char(compute='_totallot',readonly=True)
    total=fields.Char(compute='_totalHj',readonly=True)
    progress=fields.Float(compute='_progress_tr',readonly=True)
    autre=fields.Text()
    lot_ids=fields.One2many('planification.liste', 'plan_id')
    
    @api.model
    def create(self,vals):
        vals['ref']=self.env['ir.sequence'].next_by_code('planification.plan')
        res=super(Plan, self).create(vals)
        return res
    
    @api.multi
    @api.depends('hjs','lot_ids.hj')
    def _progress_tr(self):
        somme=0
        for r in self:
            if not r.hjs:
                r.progress=0.0
            else:
                for line in r.lot_ids:
                    if line.state=='finished':
                        somme+=line.hj
                r.progress=(100.0*somme)/r.hjs

    @api.onchange('lot_ids')
    def _totalHj(self):
        for r in self:
            r.total = sum(line.hj for line in r.lot_ids)    
    @api.multi
    @api.depends('lot_ids')
    def _totallot(self):
        for r in self:
            r.totalot=len(r.lot_ids)
    @api.multi
    def validate(self):
        return{
             "type": "ir.actions.act_window",
    "res_model":'project.project',
    "views": [[False, "form"]],
    "target": "new",
            }
    @api.multi
    def goto_tache(self):
        return{
            'type': 'ir.actions.act_window',
            "res_model": "planification.liste",
            "views": [[False, "tree"], [False, "form"]],
            }
    # WorkFlow
    state = fields.Selection([
        ('draft', "Draft"),
        ('etude',"Etude"),
        ('confirmed', "Confirmed"),
        ('trait',"Traitement"),
        ('done', "Done"),
    ], default='draft')
    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirmed'

    @api.multi
    def action_done(self):
        self.state = 'done'
    
    @api.multi
    def action_etude(self):
        self.state='etude'
    
    @api.multi
    def action_trait(self):
        self.state='trait'
    
class Liste(models.Model):
    _name='planification.liste'
    _description='liste des lots'
    lot=fields.Char(required=True)
    tache=fields.Char(required=True,sum='Total')
    #fields.Many2one(comodel_name='project.task',required=True ,sum='Total')
    hj=fields.Integer(required=True)
    deadline=fields.Date(index=True,copy=False,required=True)
    state=fields.Selection([
        ('ongoing',"Ongoing"),('finished',"Finished")
    ],default='ongoing')
    plan_id=fields.Many2one('planification.plan','Plan')
    
    @api.multi
    def action_finished(self):
        self.state='finished'
    @api.multi
    def action_ongoing(self):
        self.state='ongoing'
class Tache(models.Model):
    _name='planification.tache'
    _description='Tache'
    nom=fields.Char(required=True,help="Nom de la tache")
    hj=fields.Integer(required=True)
    deadline=fields.Date(index=True,copy=False,required=True)
    origine=fields.Char(required=True,help="Nom du lot")

# class ir_sequence_type(models.Model):
#     _name='ir.sequence.type'
#     @api.model
#     def create(self,vals):
#         vals['ref']=self.env['ir.sequence'].next_by_code('planification.plan')
#         res=super(Plan, self).create(vals)
#         return res