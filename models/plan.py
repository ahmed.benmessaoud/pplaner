# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import date
from odoo.api import *
class Plan(models.Model):
    _name = 'planification.plan'
    _description = 'Plan de Charge'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _defaults = { 'ref': lambda self,cr,uid,context: self.pool.get('ir.sequence').get(cr, uid, 'planification.plan') }
    
    nom=fields.Char(required=True)
    client=fields.Many2one(comodel_name='res.partner',required=True)
    date_start = fields.Date(default=fields.Date.today(),index=True, copy=False)
    version = fields.Selection([('1.0','1.0'),('2.0','2.0'),('3.0','3.0'),('4.0','4.0'),('5','5')])
    ref=fields.Char('Ref',readonly=True)
    hjs=fields.Integer(required=True)
    totalot=fields.Char(compute='_totallot',readonly=True)
    total=fields.Char(compute='_totalHj',readonly=True)
    progress=fields.Float(compute='_progress_tr',readonly=True)
    autre=fields.Text()
    project_id= fields.Many2one('project.project','Project')
    lot_ids=fields.One2many('planification.liste', 'plan_id')
    color=fields.Integer()
    project_id=fields.Many2one('project.project','Project')
    @api.model
    def create(self,vals):
        vals['ref']=self.env['ir.sequence'].next_by_code('planification.plan')
        res=super(Plan, self).create(vals)
        return res
    
    @api.multi
    @api.depends('hjs','lot_ids.hj')
    def _progress_tr(self):
        somme=0
        for r in self:
            if not r.hjs:
                r.progress=0.0
            else:
                for line in r.lot_ids:
                    if line.state=='finished':
                        somme+=line.hj
                r.progress=(100.0*somme)/r.hjs

    @api.onchange('lot_ids')
    def _totalHj(self):
        for r in self:
            r.total = sum(line.hj for line in r.lot_ids)    
    @api.multi
    @api.depends('lot_ids')
    def _totallot(self):
        for r in self:
            r.totalot=len(r.lot_ids)
    @api.multi
    def goto_tache(self):
        return{
            'type': 'ir.actions.act_window',
            "res_model": "planification.liste",
            "views": [[False, "tree"], [False, "form"]],
            }
    # WorkFlow
    state = fields.Selection([
        ('draft', "Draft"),
        ('etude',"Etude"),
        ('confirmed', "Confirmed"),
        ('trait',"Traitement"),
        ('done', "Done"),
    ], default='draft')
    
    @api.one
    def action_email_send(self):
#         '''
#         This function opens a window to compose an email, with the edi sale template message loaded by default
#         '''
#         self.ensure_one()
#         ir_model_data = self.env['ir.model.data']
#         try:
#             template_id = ir_model_data.get_object_reference('sale', 'email_template_edi_sale')[1]
#         except ValueError:
#             template_id = False
#         try:
#             compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
#         except ValueError:
#             compose_form_id = False
#         ctx = dict()
#         ctx.update({
#             'default_model': 'sale.order',
#             'default_res_id': self.ids[0],
#             'default_use_template': bool(template_id),
#             'default_template_id': template_id,
#             'default_composition_mode': 'comment',
#             'mark_so_as_sent': True,
#             'custom_layout': "sale.mail_template_data_notification_email_sale_order"
#         })
#         return {
#             'type': 'ir.actions.act_window',
#             'view_type': 'form',
#             'view_mode': 'form',
#             'res_model': 'mail.compose.message',
#             'views': [(compose_form_id, 'form')],
#             'view_id': compose_form_id,
#             'target': 'new',
#             'context': ctx,
#         }#
#         template_obj = self.pool.get('email.template')
#         group_model_id = self.pool.get('ir.model').search([('model', '=', 'planification.plan')])
#         body_html = '''Message whatever you want to send'''
#         template_data = {
#             'model_id': group_model_id,
#             'name': 'Template Name',
#             'subject' : 'Subject for your email',
#             'body_html': body_html,
#             'email_from' : '${object.user_id.email}',
#             'email_to' : '${object.partner_id.email}',
#         }
# 
#         template_id = template_obj.create(cr, template_data)
#         template_obj.send_mail(cr, template_id, force_send=True)
# # #        template = self.env['ir.model.data'].get_object('mail_template_demo', 'example_email_template')
        #template=self.env.ref('planification.email_template_edi_plan_charge')
#         template=self.env['ir.model.data'].get_object('Planification','email_template_edi_plan_charge')
#         self.env['mail.template'].browse(template.id).send_mail(self.id)
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('Planification', 'email_template_edi_plan_charge')#[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')#s[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'planification.plan',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'custom_layout': "mail_template_data_notification_email_plan"
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
           'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }
    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirmed'
        self.action_email_send()
    @api.multi
    def action_done(self):
        self.state = 'done'
        self.action_email_send
    @api.multi
    def action_etude(self):
        self.state='etude'
        self.action_email_send
    @api.multi
    def action_trait(self):
        self.state='trait'
        self.action_email_send()
    @api.multi
    def create_project(self,vals):
        for line in self.browse():
            vals['name']=line.nom
            project_id=self.pool.get('project.project').create(vals)
        return project_id
#     @api.multi
#     def create_task_diagnostic(self):
#         line=self.browse(self.ids[0])
#         vals={'name':line.name+' '+'suivi promoteur',
#               'project_id':line.project_id.id,
#               'user_id':line.consultant_id.user_id.id if line.consultant_id.user_id else False,
#               }
#         task_id=self.pool.get('project.task').create(vals)
#         return {}
    @api.multi
    def validate(self):
        project_id=self.create_project(self)
        line=self.browse(self.ids[0])
        line.project_id=project_id
        self.create_task_diagnostic()
        return True
class Liste(models.Model):
    _name='planification.liste'
    _description='liste des lots'
    lot=fields.Char(required=True)
    tache=fields.Char(required=True,sum='Total')
    hj=fields.Integer(required=True)
    deadline=fields.Date(index=True,copy=False,required=True)
    state=fields.Selection([
        ('ongoing',"Ongoing"),('finished',"Finished")
    ],default='ongoing')
    plan_id=fields.Many2one('planification.plan','Plan')
    
    @api.multi
    def action_finished(self):
        self.state='finished'
    @api.multi
    def action_ongoing(self):
        self.state='ongoing'
class Tache(models.Model):
    _name='planification.tache'
    _description='Tache'
    nom=fields.Char(required=True,help="Nom de la tache")
    hj=fields.Integer(required=True)
    deadline=fields.Date(index=True,copy=False)
    origine=fields.Char(required=True,help="Nom du lot")

# class ir_sequence_type(models.Model):
#     _name='ir.sequence.type'
#     @api.model
#     def create(self,vals):
#         vals['ref']=self.env['ir.sequence'].next_by_code('planification.plan')
#         res=super(Plan, self).create(vals)
#         return res